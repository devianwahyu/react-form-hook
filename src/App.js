import React, { useState } from "react";
import "./index.css";

export default function App() {
  const [values, setValues] = useState({
    firstName: '',
    lastName: '',
    email: '',
  });

  const [submitted, setSubmitted] = useState(false);
  const [checked, setChecked] = useState(false);

  return (
    <div class="form-container">
      <form class="register-form">
        {submitted && <div class="success-message">Success! Thank you for registering</div>}
        <input
          value={values.firstName}
          onChange={(e) => setValues({ ...values, firstName: e.currentTarget.value })}
          id="first-name"
          class="form-field"
          type="text"
          placeholder="First Name"
          name="firstName"
        />
        {(checked && !values.firstName) && <span id="first-name-error">Please enter a first name</span>}
        <input
          value={values.lastName}
          onChange={(e) => setValues({ ...values, lastName: e.currentTarget.value })}
          id="last-name"
          class="form-field"
          type="text"
          placeholder="Last Name"
          name="lastName"
        />
        {(checked && !values.lastName) && <span id="last-name-error">Please enter a last name</span>}
        <input
          value={values.email}
          onChange={(e) => setValues({ ...values, email: e.currentTarget.value })}
          id="email"
          class="form-field"
          type="text"
          placeholder="Email"
          name="email"
        />
        {(checked && !values.email) && <span id="email-error">Please enter an email address</span>}
        <button class="form-field" type="submit" onClick={(e) => {
          e.preventDefault();
          setSubmitted(values.firstName && values.lastName && values.email);
          setChecked(true);
        }}>
          Register
        </button>
      </form>
    </div>
  );
}